using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Continue : MonoBehaviour
{

    private PlayerControls controls;
    private void Awake() 
    {
        controls = new PlayerControls();
        
        controls.Gameplay.Continue.performed += context => loadNextScene();
        controls.Gameplay.Shoot.performed += context => loadNextScene();
    }

    private void loadNextScene() 
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    private void OnEnable()
    {
        controls.Enable();
    }
    private void OnDisable()
    {
        controls.Disable();
    }
}
