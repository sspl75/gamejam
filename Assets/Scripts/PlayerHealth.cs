using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    //state
    private int currentHealth;
    private PlayerMovement player;
    private PlayerShoot playerShooting;
    private Animator animator;
    [SerializeField]
    private Canvas gameOverCanvas;
    //config
    [SerializeField]
    private int maxHealth = 3;
    [SerializeField]
    private Slider healthBar;


    private void Start()
    {
        currentHealth = maxHealth;
        healthBar.maxValue = maxHealth;
        healthBar.value = currentHealth;
        player = GetComponent<PlayerMovement>();
        playerShooting = GetComponent<PlayerShoot>();
        animator = GetComponent<Animator>();
    }

    public void LooseHealth(int damage)
    {
        currentHealth -= damage;
        healthBar.value = currentHealth;
        if (currentHealth <= 0)
        {
            Die();
            return;
        }
        animator.SetTrigger("PlayerHit");

    }

    private void Die()
    {
        player.DisableControls();
        playerShooting.DisableControls();
        animator.SetTrigger("PlayerDeath");
        StartCoroutine(GameOver());
    }

    private IEnumerator GameOver()
    {
        yield return new WaitForSeconds(animator.GetCurrentAnimatorStateInfo(0).length);
        gameOverCanvas.enabled = true;
        Time.timeScale = 0;
    }

    public int GetMaxHealth() => maxHealth;
    public int GetCurrentHealth() => currentHealth;
    public void SetMaxHealth(int newHealth)
    {
        maxHealth = newHealth;
        healthBar.maxValue = newHealth;
    }

    public void SetCurrentHealthToMaxHealth()
    {
        currentHealth = maxHealth;
        healthBar.value = currentHealth;
    }
}