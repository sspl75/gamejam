using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    // Config
    [SerializeField]
    private GameObject enemyPrefab;
    [SerializeField]
    private bool walkLeft = false;
    [SerializeField]
    private float minimumSpawnInterval = 5f;
    [SerializeField]
    private float maximumSpawnInterval = 10f;

    // States
    private float timeTillNextSpawn;

    // Start is called before the first frame update
    void Start()
    {
        Spawn();
        timeTillNextSpawn = Random.Range(minimumSpawnInterval, maximumSpawnInterval);
    }

    // Update is called once per frame
    void Update()
    {
        if (timeTillNextSpawn <= 0)
        {
            Spawn();
            timeTillNextSpawn = Random.Range(minimumSpawnInterval, maximumSpawnInterval);
        }
        else timeTillNextSpawn -= Time.deltaTime;
        
    }

    public void Spawn()
    {
        GameObject newEnemy = Instantiate(enemyPrefab, transform.position, enemyPrefab.transform.rotation);
        if(walkLeft)
        {
            Enemy enemyComponent = newEnemy.GetComponent<Enemy>();
            if (enemyComponent != null) enemyComponent.ChangeDirection();
        }
    }
}
