using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CoreGameManager : MonoBehaviour
{
    // Config
    [SerializeField]
    private GameObject[] spawnPoints;
    [SerializeField]
    private int nextSpawnPointActivation = 200;
    [SerializeField]
    private TextMeshProUGUI scoreLabel;

    [Header("Upgrades")]
    [SerializeField]
    private int ammoIncrease = 3;
    [SerializeField]
    private int healthIncrease = 1;
    [SerializeField]
    private int damageIncrease = 1;
    [SerializeField]
    private TextMeshProUGUI upgradeInfoLabel;
    [SerializeField]
    private float upgradeInfoDisplayTime = 3f;

    // states
    private int leftPointsTillNextActivation;
    private int currentScore = 0;
    private int nextSpawnPointIndex = 2;
    private int currentDamageModificator = 0;

    
    // Cached References
    private PlayerHealth playerHealth;
    private PlayerShoot playerShooting;

    // Start is called before the first frame update
    void Start()
    {
        playerHealth = FindObjectOfType<PlayerHealth>();
        playerShooting = FindObjectOfType<PlayerShoot>();
        leftPointsTillNextActivation = nextSpawnPointActivation;
        scoreLabel.text = currentScore.ToString();
    }

    // Update is called once per frame
    public void IncreaseScore(int points)
    {

        currentScore += points;
        scoreLabel.text = currentScore.ToString();
        leftPointsTillNextActivation -= points;
        if(leftPointsTillNextActivation <= 0)
        {
           if (nextSpawnPointIndex < spawnPoints.Length) spawnPoints[nextSpawnPointIndex++].SetActive(true);
            leftPointsTillNextActivation = nextSpawnPointActivation;
            PerformUpgrade();
        }
    }

    private void PerformUpgrade()
    {
        int choosenUpgrade = Random.Range(1, 4);
        switch(choosenUpgrade)
        {
            case 1:
                MaxHealthUpgrade();
                break;
            case 2:
                MaxAmmoUpgrade();
                break;
            case 3:
                DamageIncreaseUpgrade();
                break;
            default:
                Debug.LogError("Unknown upgrade selected.");
                break;
        }
    }

    private void MaxHealthUpgrade()
    {
        playerHealth.SetMaxHealth(playerHealth.GetCurrentHealth() + healthIncrease);
        playerHealth.SetCurrentHealthToMaxHealth();
        StartCoroutine(PrintUpgradeMessage("You have been healed and max health has increased!"));
    }

    private void MaxAmmoUpgrade()
    {
        playerShooting.SetMaxAmmoCount(playerShooting.GetMaxAmmo() + ammoIncrease);
        playerShooting.SetCurrentAmmoToMaxAmmo();
        StartCoroutine(PrintUpgradeMessage("Gun has been reloaded and max ammo has been increased!"));
    }

    private void DamageIncreaseUpgrade()
    {
        currentDamageModificator += damageIncrease;
        StartCoroutine(PrintUpgradeMessage("Your damage has increased!"));
    }

    public int GetDamageModificator() => currentDamageModificator;

    private IEnumerator PrintUpgradeMessage(string message)
    {
        upgradeInfoLabel.enabled = true;
        upgradeInfoLabel.text = "Upgrade received: " + message;
        yield return new WaitForSeconds(upgradeInfoDisplayTime);
        upgradeInfoLabel.enabled = false;
    }
}
