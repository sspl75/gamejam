using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerShoot : MonoBehaviour
{
    //cached references
    private PlayerControls controls;
    private Animator animator;
    private PlayerMovement movement;

    //state
    private bool shooting;
    private int currentAmmoCount;
    private float leftTimeForReload = 0;
    private bool reloading = false;
    private bool reloadSFXPlaying = false;
    private Coroutine currentSpawnBulletInstance;

    //config
    [Header("Shooting")]
    [SerializeField]
    private float bulletSpawnInterval = 0.5f;
    [SerializeField]
    private float shootingRunModifier = 0.66f;
    [SerializeField]
    private int maxAmmoCount = 10;
    [SerializeField]
    private float reloadTime = 3;

    [Header("Manual References")]
    [SerializeField]
    private GameObject bulletPrefab;
    [SerializeField]
    private Transform shootPoint;

    [Header("SFX")]
    [SerializeField]
    private AudioClip shootSFX;
    [SerializeField]
    private float shootSFXVolume = 1f;
    [SerializeField]
    private AudioClip emptyShootSFX;
    [SerializeField]
    private float emptyShootSFXVolume = 1f;
    [SerializeField]
    private AudioClip startReloadSFX;
    [SerializeField]
    private float startReloadSFXVolume = 1f;
    [SerializeField]
    private AudioClip finishReloadSFX;
    [SerializeField]
    private float finishReloadSFXVolume = 1f;

    [Header("UI")]
    [SerializeField]
    private TextMeshProUGUI currentAmmoLabel;


    private void Awake()
    {
        controls = new PlayerControls();

        controls.Gameplay.Shoot.performed += context => StartShooting();
        controls.Gameplay.Shoot.canceled += context => StopShooting();
        controls.Gameplay.Reload.performed += context => StartReloading();
    }

    private void Start()
    {
        animator = GetComponent<Animator>();
        movement = GetComponent<PlayerMovement>();
        currentAmmoCount = maxAmmoCount;
        currentAmmoLabel.text = currentAmmoCount.ToString();
        leftTimeForReload = reloadTime;
    }

    private void Update()
    {
        if (reloading) ReloadGun();
    }

    private void StartShooting()
    {
        shooting = true;
        currentSpawnBulletInstance = StartCoroutine(SpawnBullet());
        movement.SetRunSpeedModifier(shootingRunModifier);

        //animation
        animator.SetBool("Shooting", true);
    }

    private void StopShooting()
    {
        shooting = false;
        StopCoroutine(currentSpawnBulletInstance);
        movement.ResetRunSpeedModifier();

        //animation
        animator.SetBool("Shooting", false);
    }

    private IEnumerator SpawnBullet()
    {
        if(currentAmmoCount-- > 0 && !reloading)
        {
            currentAmmoLabel.text = currentAmmoCount.ToString();
            Instantiate(bulletPrefab, shootPoint.position, shootPoint.rotation);
            AudioSource.PlayClipAtPoint(shootSFX, transform.position, shootSFXVolume);
        }
        AudioSource.PlayClipAtPoint(emptyShootSFX, transform.position, emptyShootSFXVolume);
        yield return new WaitForSeconds(bulletSpawnInterval);
        if (shooting) StartCoroutine(SpawnBullet());
    }

    private void ReloadGun()
    {
        if (leftTimeForReload <= 0)
        {
            currentAmmoCount = maxAmmoCount;
            leftTimeForReload = reloadTime;
            currentAmmoLabel.text = currentAmmoCount.ToString();
            reloading = false;
            reloadSFXPlaying = false;
            
        } 
        else if (!reloadSFXPlaying && leftTimeForReload <= finishReloadSFX.length ) 
        {
            reloadSFXPlaying = true;
            animator.SetTrigger("Reloaded");
            AudioSource.PlayClipAtPoint(finishReloadSFX, transform.position, finishReloadSFXVolume);
        }
        else leftTimeForReload -= Time.deltaTime;
    }


    private void StartReloading()
    {
        if (!reloading) 
        {
            AudioSource.PlayClipAtPoint(startReloadSFX, transform.position, startReloadSFXVolume);
            reloading = true;
        }
    }

    private void OnEnable()
    {
        controls.Enable();
    }

    public void DisableControls()
    {
        controls.Disable();
    }
    private void OnDisable()
    {
        DisableControls();
    }

    public int GetMaxAmmo() => maxAmmoCount;
    public void SetMaxAmmoCount(int newValue)
    {
        maxAmmoCount = newValue;
    }

    public void SetCurrentAmmoToMaxAmmo()
    {
        currentAmmoCount = maxAmmoCount;
        currentAmmoLabel.text = currentAmmoCount.ToString();
    }
}
