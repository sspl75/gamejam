using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    //cached references
    private Rigidbody2D rb;
    private Animator animator;
    private CoreGameManager gameManager;
    private AudioSource walkAudioSource;

    //state
    private float attackCooldownTimer = 0;
    [SerializeField]
    private int healthPoints = 3;
    private bool isDead = false;

    //config
    [Header("Movement Parameters")]
    [SerializeField]
    private float moveSpeed = 100.0f;

    [SerializeField] private float turnDistance = 1.0f;
    [SerializeField] private LayerMask obstacles;

    [Header("Attacking Parameters")]
    [SerializeField]
    private LayerMask targetLayers;
    [SerializeField] 
    private int attackDamage;
    [SerializeField] 
    private LayerMask visibleLayers;
    [SerializeField]
    private float timeBetweenAttacks = 3.0f;

    [Header("Manual References")]
    [SerializeField]
    private Transform scanPoint;
    [SerializeField]
    private float afterDeathDelay;

    [Header("Score Parameters")]
    [SerializeField]
    private int pointsForThisEnemy = 10;

    private PlayerHealth playerHealth;

    [Header("SFX")]
    [SerializeField]
    private float minIntervalbetweenWalkSound = 3f;
    [SerializeField]
    private float maxIntervalbetweenWalkSound = 5f;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        playerHealth = FindObjectOfType<PlayerHealth>();
        if (playerHealth == null)
        {
            throw new NoNullAllowedException();
        }
        gameManager = FindObjectOfType<CoreGameManager>();
        if (gameManager == null)
        {
            throw new NoNullAllowedException();
        }
        walkAudioSource = GetComponent<AudioSource>();
        StartCoroutine(PlayWalkSounds());
    }

    private void FixedUpdate()
    {
        if (PlayerVisible() && attackCooldownTimer <= 0)
        {
            attackCooldownTimer = timeBetweenAttacks;
            StartCoroutine(Attacking());
            
        }
        else if (attackCooldownTimer > 0)
        {
            attackCooldownTimer -= Time.fixedDeltaTime;
        }

        if (attackCooldownTimer > 0 || healthPoints <= 0)
        {
            return;
        }
        Move();
    }

    private void Move()
    {
        float horizontalVelocity = transform.right.x * moveSpeed * Time.fixedDeltaTime;
        rb.velocity = new Vector2(horizontalVelocity, rb.velocity.y);
    }

    public void ChangeDirection()
    {
        if (transform.eulerAngles == Vector3.zero) transform.eulerAngles = new Vector3(0.0f, 180.0f, 0.0f);
        else transform.eulerAngles = Vector3.zero;
    }

    private bool WallOrGapAhead()
    {
        RaycastHit2D wallHit = Physics2D.Raycast(scanPoint.position, transform.right, turnDistance, obstacles);
        RaycastHit2D floorHit = Physics2D.Raycast(scanPoint.position, -transform.up, scanPoint.localPosition.y + 0.5f,
            obstacles);
        return wallHit.collider != null || floorHit.collider == null;
    }

    private bool PlayerVisible()
    {
        bool playerHit = false;
        RaycastHit2D hit = Physics2D.Raycast(scanPoint.position, transform.right, 2.5f, visibleLayers);
        if (hit.collider != null)
        {
            playerHit = (targetLayers.value & (1 << hit.collider.gameObject.layer)) > 0;
        }

        return playerHit;
    }

    private IEnumerator Attacking()
    {
        animator.SetTrigger("EnemyAttack");
        yield return new WaitForSeconds(0.65f);
        if (PlayerVisible() && healthPoints > 0 ) playerHealth.LooseHealth(attackDamage);
    }

    public void TakeDamage(int damage)
    {
        healthPoints -= damage;
        if (healthPoints <= 0)
        {
            Die();
            return;
        }
        animator.SetTrigger("EnemyHit");
    }

    public void Die()
    {
        
        if (!isDead) 
        {
            isDead = true;
            animator.SetTrigger("EnemyDeath");
            gameManager.IncreaseScore(pointsForThisEnemy);
            Destroy(gameObject, animator.GetCurrentAnimatorStateInfo(0).length);
        }
    }

    private IEnumerator PlayWalkSounds()
    {
        walkAudioSource.Play();
        yield return new WaitForSeconds(Random.Range(minIntervalbetweenWalkSound, maxIntervalbetweenWalkSound));
        if (healthPoints > 0) StartCoroutine(PlayWalkSounds());
    }
}
