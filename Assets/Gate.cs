using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gate : MonoBehaviour
{
    [SerializeField]
    private LayerMask targetLayers;
    [SerializeField]
    private int maxHealth = 3;
    [SerializeField]
    private Canvas gameOverCanvas;
    [SerializeField]
    private Slider healthBar;

    // States
    private int currentHealth;

    private void Start()
    {
        currentHealth = maxHealth;
        healthBar.maxValue = maxHealth;
        healthBar.value = currentHealth;
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if ((targetLayers.value & (1 << collision.gameObject.layer)) <= 0) return;
        currentHealth -= 1;
        healthBar.value = currentHealth;
        Destroy(collision.gameObject);
        if (currentHealth <= 0)
        {
            GameOver();
        }
    }

    private void GameOver()
    {
        PlayerMovement playerMovement = FindObjectOfType<PlayerMovement>();
        PlayerShoot playerShooting = FindObjectOfType<PlayerShoot>();
        if(playerShooting != null && playerMovement != null)
        {
            playerMovement.DisableControls();
            playerShooting.DisableControls();
        }
        gameOverCanvas.enabled = true;
        Time.timeScale = 0;
    }
}
